﻿using AutoMapper;
using Entities.DBModels;
using Entities.DTOModels;

namespace BusinessLogicLayer.Test.IoC
{
    public class Mapping : Profile
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<GroupDTO, Group>();
                cfg.CreateMap<Group, GroupDTO>()
                    .ForMember(x => x.Id, y => y.MapFrom(t => t.Id))
                    .ForMember(x => x.Name, y => y.MapFrom(t => t.Name));

                cfg.CreateMap<StudentDTO, Student>();
                cfg.CreateMap<Student, StudentDTO>()
                    .ForMember(x => x.Name, y => y.MapFrom(t => t.Name))
                    .ForMember(x => x.Surname, y => y.MapFrom(t => t.Surname))
                    .ForMember(x => x.DateOfBirthday, y => y.MapFrom(t => t.DateOfBirthday))
                    .ForMember(x => x.Gender, y => y.MapFrom(t => t.Gender))
                    .ForMember(x => x.GroupId, y => y.MapFrom(t => t.Group.Id));
            });
        }
    }
}
