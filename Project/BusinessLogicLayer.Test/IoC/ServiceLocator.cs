﻿using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Managers;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Entities.DBModels;
using Entities.DTOModels;
using Unity;

namespace BusinessLogicLayer.Test.IoC
{
    public class ServiceLocator
    {
        private readonly IUnityContainer _container;

        private static ServiceLocator _instance;

        public static ServiceLocator Instance => _instance ?? (_instance = new ServiceLocator());
        
        public ServiceLocator()
        {
            _container = new UnityContainer();
            InitializeDataAccessLayer(_container);
            InitializeBusinessLogic(_container);
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        private void InitializeDataAccessLayer(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager());

            container.RegisterType<IBaseRepository<Group>, GroupRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBaseRepository<Student>, StudentRepository>(new ContainerControlledLifetimeManager());
        }

        private void InitializeBusinessLogic(IUnityContainer container)
        {
            container.RegisterType<IValidator, AttributeValidator>(new ContainerControlledLifetimeManager());

            container.RegisterType<IBaseManager<GroupDTO>, GroupManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBaseManager<StudentDTO>, StudentManager>(new ContainerControlledLifetimeManager());
        }
    }
}
