﻿using NUnit.Framework;

namespace BusinessLogicLayer.Test.ManagerTests
{
    public class BaseTest<T>
    {
        protected T service;

        [SetUp]
        public void SetUp()
        {
            Init();
        }
        protected virtual void Init()
        {

        }
    }
}
