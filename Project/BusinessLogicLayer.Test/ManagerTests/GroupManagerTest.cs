﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Managers;
using BusinessLogicLayer.Test.IoC;
using DataAccessLayer.Interfaces;
using Entities.DBModels;
using Entities.DTOModels;
using Moq;
using NUnit.Framework;

namespace BusinessLogicLayer.Test.ManagerTests
{
    public class GroupManagerTest:BaseTest<IBaseManager<GroupDTO>>
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IBaseManager<GroupDTO> _groupManager;
        private GroupDTO _group;
        private Group _groupDb;
        private IEnumerable<Group> _groups;

        protected override void Init()
        {
            Mapping.Configure();
            _group = new GroupDTO {Id = 1, Name = "203"};
            _groupDb = new Group { Id = 1, Name = "203" };
            _groups = new List<Group> {_groupDb};
            _unitOfWork = new Mock<IUnitOfWork>();
            _groupManager = new GroupManager(_unitOfWork.Object, ServiceLocator.Instance.Resolve<IValidator>());
        }
        
        [Test]
        public void CreateGroup_NameIsEmpty_ThrowsValidationException()
        {
            _group.Name = string.Empty;
            Assert.That(() => _groupManager.Create(_group), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateGroup_NameIsNull_ThrowsValidationException()
        {
            _group.Name = null;
            Assert.That(() => _groupManager.Create(_group), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateGroup_NameHasMoreThan20Characters_ThrowsValidationException()
        {
            _group.Name = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethnoe" +
                          "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _groupManager.Create(_group), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateGroup_NameIsEmpty_ThrowsValidationException()
        {
            _group.Name = string.Empty;
            Assert.That(() => _groupManager.Update(_group), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateGroup_NameIsNull_ThrowsValidationException()
        {
            _group.Name = null;
            Assert.That(() => _groupManager.Update(_group), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateGroup_NameHasMoreThan20Characters_ThrowsValidationException()
        {
            _group.Name = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethe" +
                          "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _groupManager.Update(_group), Throws.TypeOf<ValidationException>());
        }
        
        [Test]
        public void GetGroupById_IdGroupIsNotValid_GroupIsNotFound_ThrowsArgumentException()
        {
            Assert.That(() => _groupManager.GetById(-2), Throws.TypeOf<ArgumentException>());
        }
        
        [Test]
        public void GetAllGroups_GroupsIsNotFound_ReturnsEmptyList()
        {
            _groups = new List<Group>();
            _unitOfWork.Setup(u => u.GroupRepository.GetAll()).Returns(_groups);
            var groups = _groupManager.GetAll().ToList();
            Assert.AreEqual(groups.Count, 0);
        }

        [Test]
        public void DeleteGroup_IdGroupIsNegative_ThrowsArgumentException()
        {
            var idGroup = -10;
            Assert.That(() => _groupManager.Delete(idGroup), Throws.TypeOf<ArgumentException>());
        }
    }
}
