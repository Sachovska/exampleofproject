﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Managers;
using BusinessLogicLayer.Test.IoC;
using DataAccessLayer.Interfaces;
using Entities.DBModels;
using Entities.DTOModels;
using Moq;
using NUnit.Framework;

namespace BusinessLogicLayer.Test.ManagerTests
{
    public class StudentManagerTest : BaseTest<IBaseManager<StudentDTO>>
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IBaseManager<StudentDTO> _studentManager;
        private StudentDTO _student;
        private Student _studentDb;
        private IEnumerable<Student> _students;
        private Group _groupDb;

        protected override void Init()
        {
            Mapping.Configure();
            _groupDb = new Group { Id = 1, Name = "203" };
            _student = new StudentDTO
            {
                Id = 1,
                Name = "Vitalina",
                Surname = "Sachovska",
                Gender = "m",
                DateOfBirthday = Convert.ToDateTime("1998-04-07"),
                GroupId = 1
            };
            _studentDb = new Student
            {
                Id = 1,
                Name = "Vitalina",
                Surname = "Sachovska",
                Gender = "m",
                DateOfBirthday = Convert.ToDateTime("1998-04-07"),
                Group = _groupDb
            };
            _students = new List<Student> {_studentDb};
            _unitOfWork = new Mock<IUnitOfWork>();
            _studentManager = new StudentManager(_unitOfWork.Object, ServiceLocator.Instance.Resolve<IValidator>());
        }

        [Test]
        public void CreateStudent_NameIsEmpty_ThrowsValidationException()
        {
            _student.Name = string.Empty;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateGroup_NameIsNull_ThrowsValidationException()
        {
            _student.Name = null;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_NameHasMoreThan20Characters_ThrowsValidationException()
        {
            _student.Name = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethnoe" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_SurnameIsEmpty_ThrowsValidationException()
        {
            _student.Surname = string.Empty;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_SurnameIsNull_ThrowsValidationException()
        {
            _student.Surname = null;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_SurnameHasMoreThan20Characters_ThrowsValidationException()
        {
            _student.Surname = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethnoe" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }
        
        [Test]
        public void CreateStudent_DateOfBirthdayIsNull_ThrowsValidationException()
        {
            _student.DateOfBirthday = null;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_DateOfbirthdayIsInvalid_ThrowsValidationException()
        {
            _student.DateOfBirthday = Convert.ToDateTime("2020-10-20");
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_GenderIsEmpty_ThrowsValidationException()
        {
            _student.Gender = string.Empty;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_GenderIsNull_ThrowsValidationException()
        {
            _student.Gender = null;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_GenderIsNotValidValue_ThrowsValidationException()
        {
            _student.Gender = "gender";
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void CreateStudent_GroupIdIsNotValidValue_ThrowsValidationException()
        {
            _student.GroupId = null;
            Assert.That(() => _studentManager.Create(_student), Throws.TypeOf<ValidationException>());
        }

        //

        [Test]
        public void UpdateStudent_NameIsEmpty_ThrowsValidationException()
        {
            _student.Name = string.Empty;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateGroup_NameIsNull_ThrowsValidationException()
        {
            _student.Name = null;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_NameHasMoreThan20Characters_ThrowsValidationException()
        {
            _student.Name = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethnoe" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_SurnameIsEmpty_ThrowsValidationException()
        {
            _student.Surname = string.Empty;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_SurnameIsNull_ThrowsValidationException()
        {
            _student.Surname = null;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_SurnameHasMoreThan20Characters_ThrowsValidationException()
        {
            _student.Surname = "morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethnoe" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20" +
                            "n20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20morethen20";
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }
        
        [Test]
        public void UpdateStudent_DateOfBirthdayIsNull_ThrowsValidationException()
        {
            _student.DateOfBirthday = null;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_DateOfbirthdayIsInvalid_ThrowsValidationException()
        {
            _student.DateOfBirthday = Convert.ToDateTime("2020-10-20");
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_GenderIsEmpty_ThrowsValidationException()
        {
            _student.Gender = string.Empty;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_GenderIsNull_ThrowsValidationException()
        {
            _student.Gender = null;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_GenderIsNotValidValue_ThrowsValidationException()
        {
            _student.Gender = "gender";
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void UpdateStudent_GroupIdIsNotValidValue_ThrowsValidationException()
        {
            _student.GroupId = null;
            Assert.That(() => _studentManager.Update(_student), Throws.TypeOf<ValidationException>());
        }

        [Test]
        public void GetStudentById_IdStudentIsNotValid_StudentIsNotFound_ThrowsArgumentException()
        {
            Assert.That(() => _studentManager.GetById(-2), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void GetAllStudents_StudentsNotFound_ReturnsEmptyList()
        {
            _students = new List<Student>();
            _unitOfWork.Setup(u => u.StudentRepository.GetAll()).Returns(_students);
            var students = _studentManager.GetAll().ToList();
            Assert.AreEqual(students.Count, 0);
        }

        [Test]
        public void DeleteStudent_IdStudentIsNegative_ThrowsArgumentException()
        {
            var idStudent = -10;
            Assert.That(() => _studentManager.Delete(idStudent), Throws.TypeOf<ArgumentException>());
        }
    }
}
