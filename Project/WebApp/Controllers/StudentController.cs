﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Interfaces;
using Entities.DTOModels;

namespace WebApp.Controllers
{
    /// <summary>
    /// Student controller
    /// </summary>
    public class StudentController : ApiController
    {
        private readonly IBaseManager<StudentDTO> _studentManager;

        /// <summary>
        /// Initializing manager
        /// </summary>
        /// <param name="studentManager"></param>
        public StudentController(IBaseManager<StudentDTO> studentManager)
        {
            _studentManager = studentManager;
        }

        /// <summary>
        /// Get All students
        /// </summary>
        /// <returns>List of students</returns>
        // GET: api/Student/GetAll
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var students = _studentManager.GetAll();
                return Ok(students);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Get student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Student</returns>
        // GET: api/Student/GetById?id=1
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var students = _studentManager.GetById(id);
                return Ok(students);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Create new one student
        /// </summary>
        /// <param name="student"></param>
        /// <returns>Created group</returns>
        // POST: api/Student/Create
        [HttpPost]
        public IHttpActionResult Create(StudentDTO student)
        {
            try
            {
                _studentManager.Create(student);
                return Content(HttpStatusCode.Created, student);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Update student
        /// </summary>
        /// <param name="student"></param>
        /// <returns>Updated student</returns>
        // PUT: api/Student/Update
        [HttpPut]
        public IHttpActionResult Update(StudentDTO student)
        {
            try
            {
                _studentManager.Update(student);
                return Content(HttpStatusCode.Accepted, student);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Delete student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Status</returns>
        // DELETE: api/Student/Delete?id=1
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _studentManager.Delete(id);
                return Ok();
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
