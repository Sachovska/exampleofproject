﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Interfaces;
using Entities.DTOModels;

namespace WebApp.Controllers
{
    /// <summary>
    /// Group controller
    /// </summary>
    public class GroupController : ApiController
    {
        private readonly IBaseManager<GroupDTO> _groupManager;

        /// <summary>
        /// Initializing manager
        /// </summary>
        /// <param name="groupManager"></param>
        public GroupController(IBaseManager<GroupDTO> groupManager)
        {
            _groupManager = groupManager;
        }

        /// <summary>
        /// Get all groups
        /// </summary>
        /// <returns>List of groups</returns>
        // GET: api/Group/GetAll
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var groups = _groupManager.GetAll();
                return Ok(groups);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Get group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Group</returns>
        // GET: api/Group/GetById?id=1
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var groups = _groupManager.GetById(id);
                return Ok(groups);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }


        /// <summary>
        /// Create new one group
        /// </summary>
        /// <param name="group"></param>
        /// <returns>Created group</returns>
        // POST: api/Group/Create
        [HttpPost]
        public IHttpActionResult Create(GroupDTO group)
        {
            try
            {
                _groupManager.Create(group);
                return Content(HttpStatusCode.Created, group);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="group"></param>
        /// <returns>Updated group</returns>
        // PUT: api/Group/Update
        [HttpPut]
        public IHttpActionResult Update(GroupDTO group)
        {
            try
            {
                _groupManager.Update(group);
                return Content(HttpStatusCode.Accepted, group);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        /// <summary>
        /// Delete group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Status</returns>
        // DELETE: api/Group/Delete?id=1
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _groupManager.Delete(id);
                return Ok();
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
