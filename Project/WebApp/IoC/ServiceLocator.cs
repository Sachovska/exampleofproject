﻿using System.Web.Http.Dependencies;
using BusinessLogicLayer;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Managers;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Entities.DBModels;
using Entities.DTOModels;
using Unity;

namespace WebApp.IoC
{
    /// <summary>
    /// Dependency Injection
    /// Register all dependecies on project
    /// </summary>
    public class ServiceLocator
    {
        private readonly IUnityContainer _container;

        private static ServiceLocator _instance;

        public static ServiceLocator Instance => _instance ?? (_instance = new ServiceLocator());

        public IDependencyResolver DependencyResolver => new UnityResolver(_container);

        public ServiceLocator()
        {
            _container = new UnityContainer();
            InitializeDataAccessLayer(_container);
            InitializeBusinessLogic(_container);
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
        
        /// <summary>
        /// Register all dependencies of DAL layer
        /// </summary>
        /// <param name="container">container</param>
        private void InitializeDataAccessLayer(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager());

            container.RegisterType<IBaseRepository<Group>, GroupRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBaseRepository<Student>, StudentRepository>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Register all dependencies of BLL layer
        /// </summary>
        /// <param name="container">container</param>
        private void InitializeBusinessLogic(IUnityContainer container)
        {
            container.RegisterType<IValidator, AttributeValidator>(new ContainerControlledLifetimeManager());

            container.RegisterType<IBaseManager<GroupDTO>, GroupManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBaseManager<StudentDTO>, StudentManager>(new ContainerControlledLifetimeManager());

        }
    }
}
