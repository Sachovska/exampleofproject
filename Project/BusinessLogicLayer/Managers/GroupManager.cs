﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DBModels;
using Entities.DTOModels;

namespace BusinessLogicLayer.Managers
{
    /// <summary>
    /// Implement IBaseManager interface
    /// The class impelement all CRUD methods for GroupDTO model
    /// </summary>
    public class GroupManager: IBaseManager<GroupDTO>
    {
        /// <summary>
        /// Interface of UnitOfWork
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Interface of IValidator
        /// </summary>
        private readonly IValidator _validator;

        /// <summary>
        /// Initializing private variables
        /// </summary>
        /// <param name="unitOfWork">Interface of UnitOfWork</param>
        /// <param name="validator">Interface Of Validator</param>
        public GroupManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        /// <summary>
        /// The private method that validate Id
        /// Throw ArgumentException if Id less than '1'
        /// </summary>
        /// <param name="id">Id of model</param>
        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        /// <summary>
        /// Add new group
        /// Map DTO model to DB model
        /// Save changes
        /// </summary>
        /// <param name="item">Object of group</param>
        public void Create(GroupDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.GroupRepository.Create(Mapper.Map<Group>(item));
            _unitOfWork.Save();
        }

        /// <summary>
        /// Modify group
        /// Map DTO model to DB model
        /// Save changes
        /// </summary>
        /// <param name="item">Object of group</param>
        public void Update(GroupDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.GroupRepository.Update(Mapper.Map<Group>(item));
            _unitOfWork.Save();
        }

        /// <summary>
        /// Validate id
        /// Remove group
        /// Save changes
        /// </summary>
        /// <param name="id">Primary key of group</param>
        public void Delete(int id)
        {
            ValidateId(id);
            _unitOfWork.GroupRepository.Delete(id);
            _unitOfWork.Save();
        }

        /// <summary>
        /// Select all groups
        /// Map DB models to DTO mmodels
        /// </summary>
        /// <returns>Groups (DTO model)</returns>
        public IEnumerable<GroupDTO> GetAll()
        {
            return _unitOfWork.GroupRepository.GetAll().ToList().Select(Mapper.Map<GroupDTO>).ToList();
        }

        /// <summary>
        /// Validate id
        /// Get group by primary key
        /// Map DB model to DTO model
        /// </summary>
        /// <param name="id">Primary key of group</param>
        /// <returns>Group</returns>
        public GroupDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<GroupDTO>(_unitOfWork.GroupRepository.Get(id));
        }
    }
}
