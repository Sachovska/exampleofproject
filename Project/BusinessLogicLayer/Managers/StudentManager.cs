﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DBModels;
using Entities.DTOModels;

namespace BusinessLogicLayer.Managers
{
    /// <summary>
    /// Implement IBaseManager interface
    /// The class impelement all CRUD methods for StudentDTO model
    /// </summary>
    public class StudentManager : IBaseManager<StudentDTO>
    {
        /// <summary>
        /// Interface of UnitOfWork
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Interface of IValidator
        /// </summary>
        private readonly IValidator _validator;

        /// <summary>
        /// Initializing private variables
        /// </summary>
        /// <param name="unitOfWork">Interface of UnitOfWork</param>
        /// <param name="validator">Interface Of Validator</param>
        public StudentManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        /// <summary>
        /// The private method that validate Id
        /// Throw ArgumentException if Id less than '1'
        /// </summary>
        /// <param name="id">Id of model</param>
        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        /// <summary>
        /// Add new student
        /// Map DTO model to DB model
        /// Save changes
        /// </summary>
        /// <param name="item">Object of student</param>
        public void Create(StudentDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.StudentRepository.Create(Mapper.Map<Student>(item));
            _unitOfWork.Save();
        }

        /// <summary>
        /// Modify student
        /// Map DTO model to DB model
        /// Save changes
        /// </summary>
        /// <param name="item">Object of student</param>
        public void Update(StudentDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.StudentRepository.Update(Mapper.Map<Student>(item));
            _unitOfWork.Save();
        }

        /// <summary>
        /// Validate id
        /// Remove student
        /// Save changes
        /// </summary>
        /// <param name="id">Primary key of student</param>
        public void Delete(int id)
        {
            ValidateId(id);
            _unitOfWork.StudentRepository.Delete(id);
            _unitOfWork.Save();
        }

        /// <summary>
        /// Select all students
        /// Map DB models to DTO mmodels
        /// </summary>
        /// <returns>Students (DTO model)</returns>
        public IEnumerable<StudentDTO> GetAll()
        {
            return _unitOfWork.StudentRepository.GetAll().ToList().Select(Mapper.Map<StudentDTO>).ToList();
        }

        /// <summary>
        /// Validate id
        /// Get student by primary key
        /// Map DB model to DTO model
        /// </summary>
        /// <param name="id">Primary key of student</param>
        /// <returns>Student</returns>
        public StudentDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<StudentDTO>(_unitOfWork.StudentRepository.Get(id));
        }
    }
}
