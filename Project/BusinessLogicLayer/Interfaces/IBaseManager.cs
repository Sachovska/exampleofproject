﻿using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    /// <summary>
    /// The classes that inherit this interface will be implement CRUD methods
    /// </summary>
    /// <typeparam name="T">DTO model</typeparam>
    public interface IBaseManager<T> where T: class 
    {
        void Create(T item);
        void Update(T item);
        void Delete(int id);
        IEnumerable<T> GetAll();
        T GetById(int id);
    }
}
