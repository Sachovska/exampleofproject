﻿namespace BusinessLogicLayer.Interfaces
{
    /// <summary>
    /// The class that inherit behavor of thes interface will be implement two methods.
    /// The methods will be validated set object
    /// </summary>
    public interface IValidator
    {
        bool IsValid<T>(T obj);
        void Validate<T>(T obj);
    }
}
