﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DTOModels
{
    /// <summary>
    /// The GroupDTO (Data transfer object) class.
    /// This class contains properties that will be used on Business logic layer (BLL)
    /// DTO will replace DB (DataBases) model in higher layers
    /// Id - primary key of Group model
    /// Name - unique name of Group model. Should be set. Length should be 20 characters
    /// </summary>
    public class GroupDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The group name should be set")]
        [StringLength(20, ErrorMessage = "The group name should have only 20 characters")]
        public string Name { get; set; }

        public GroupDTO(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public GroupDTO() { }
    }
}
