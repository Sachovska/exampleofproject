﻿using System;
using System.ComponentModel.DataAnnotations;
using Entities.CustomAttributes;

namespace Entities.DTOModels
{
    /// <summary>
    /// The StudentDTO (Data transfer object) class.
    /// This class contains properties that will be used on Business logic layer (BLL)
    /// DTO will replace DB (DataBases) model in higher layers
    /// Id - primary key of Student table
    /// Surname - surname of Student. Should be set. Length should be 120 characters
    /// Name - name of Student. Should be set. Length should be 120 characters
    /// DateOfBirthday - Date of birthday of student. Should be in past
    /// Gender - the sex of student. Should be 'm/f'
    /// GroupId - foreign key of model Group. Should be set
    /// </summary>
    public class StudentDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The student name should be set")]
        [StringLength(120, ErrorMessage = "The student name should have only 120 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The student surname should be set")]
        [StringLength(120, ErrorMessage = "The student surname should have only 120 characters")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "The student date of birthday should be set")]
        [DataTimePast(ErrorMessage = "The date of bithday should be on past")]
        public DateTime? DateOfBirthday { get; set; }

        [Required(ErrorMessage = "The student gender should be set")]
        [RegularExpression("^f$|^m$", ErrorMessage = "Gender can take only 'f' or 'm'")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "The group of student should be set")]
        public int? GroupId { get; set; }

        public StudentDTO(int id, string name, string surname, DateTime dateOfBirthday, string gender, int groupId)
        {
            Id = id;
            Name = name;
            Surname = surname;
            DateOfBirthday = dateOfBirthday;
            Gender = gender;
            GroupId = groupId;
        }

        public StudentDTO() { }
    }
}
