namespace Entities.DBModels
{
    public partial class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public System.DateTime DateOfBirthday { get; set; }
        public string Gender { get; set; }
        public int GroupId { get; set; }
    
        public virtual Group Group { get; set; }
    }
}
