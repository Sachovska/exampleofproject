﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.CustomAttributes
{
    /// <summary>
    /// Inherit ValidationAttribute class
    /// Custom validation attribute
    /// </summary>
    internal class DataTimePastAttribute : ValidationAttribute
    {
        /// <summary>
        /// The method will verify that the DateTime value is in past
        /// </summary>
        /// <param name="value">The value of variable</param>
        /// <returns>Result of conditional. True/False</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            var date = (DateTime) value;
            return date <= DateTime.Now;
        }
    }
}
