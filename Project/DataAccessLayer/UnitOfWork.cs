﻿using System;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Entities.DBModels;

namespace DataAccessLayer
{
    /// <summary>
    /// The class using for initializing all repositories
    /// If you want to get some of repository you should create object of UnitOfWork and call Repository
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The DB context
        /// </summary>
        private readonly UniversityContext _universityContext;

        /// <summary>
        /// The interface of GroupRepository
        /// </summary>
        private IBaseRepository<Group> _groupRepository;

        /// <summary>
        /// The interface of StudentRepository
        /// </summary>
        private IBaseRepository<Student> _studentRepository;

        /// <summary>
        /// The initializing all of private variables
        /// </summary>
        public UnitOfWork()
        {
            _universityContext = new UniversityContext();
            _groupRepository = new GroupRepository(_universityContext);
            _studentRepository = new StudentRepository(_universityContext);
        }

        /// <summary>
        /// The inherit method that need to save chages in DB Context
        /// Should use for update/delete/create
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
           return _universityContext.SaveChanges();
        }

        private bool _disposed;

        /// <summary>
        /// Virtual method that will be cleanup resourses
        /// </summary>
        /// <param name="disposing">Value of disposed variable</param>
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _universityContext.Dispose();
                }
                _disposed = true;
            }
        }

        /// <summary>
        /// The property that initialize GroupRepository
        /// </summary>
        public IBaseRepository<Group> GroupRepository
        {
            get
            {
                if (_groupRepository != null) _groupRepository = new GroupRepository(_universityContext);
                return _groupRepository;
            }
        }

        /// <summary>
        /// The property that initialize StudentRepository
        /// </summary>
        public IBaseRepository<Student> StudentRepository
        {
            get
            {
                if (_studentRepository != null) _studentRepository = new StudentRepository(_universityContext);
                return _studentRepository;
            }
        }

        /// <summary>
        /// Clenup resourses in any case
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
