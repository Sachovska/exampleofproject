﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using DataAccessLayer.Interfaces;
using Entities.DBModels;

namespace DataAccessLayer.Repositories
{
    /// <summary>
    /// Implement methods of interface IBaseRepository
    /// </summary>
    public class GroupRepository : IBaseRepository<Group>
    {
        /// <summary>
        /// DB context
        /// </summary>
        private readonly UniversityContext _universityContext;

        /// <summary>
        /// Initializing context of DB
        /// </summary>
        /// <param name="universityContext">Context</param>
        public GroupRepository(UniversityContext universityContext)
        {
            _universityContext = universityContext;
        }

        /// <summary>
        /// Select all groups that existing in our DB
        /// </summary>
        /// <returns>list of groups</returns>
        public IEnumerable<Group> GetAll()
        {
            return _universityContext.Groups;
        }

        /// <summary>
        /// Select group by id - primary key
        /// </summary>
        /// <param name="id">Primary key of group</param>
        /// <returns>Unique group</returns>
        public Group Get(int id)
        {
            return _universityContext.Groups.Find(id);
        }

        /// <summary>
        /// Add new group to DB
        /// </summary>
        /// <param name="item">Object of group</param>
        public void Create(Group item)
        {
            _universityContext.Groups.Add(item);
        }

        /// <summary>
        /// Modify current group
        /// </summary>
        /// <param name="item">Object of group that should be updated</param>
        public void Update(Group item)
        {
            _universityContext.Groups.AddOrUpdate(item);
        }

        /// <summary>
        /// Remove group by id-primary key
        /// </summary>
        /// <param name="id">Primary key of group</param>
        public void Delete(int id)
        {
            var group = _universityContext.Groups.Find(id);
            if (group != null)
                _universityContext.Groups.Remove(group);
        }
    }
}
