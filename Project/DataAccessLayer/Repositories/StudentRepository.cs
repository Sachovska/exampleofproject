﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using DataAccessLayer.Interfaces;
using Entities.DBModels;

namespace DataAccessLayer.Repositories
{
    /// <summary>
    /// StudentRepository
    /// Implement methods of interface IBaseRepository
    /// </summary>
    public class StudentRepository:IBaseRepository<Student>
    {
        /// <summary>
        /// Private variable with DB context
        /// </summary>
        private readonly UniversityContext _universityContext;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="universityContext">Context</param>
        public StudentRepository(UniversityContext universityContext)
        {
            _universityContext = universityContext;
        }

        /// <summary>
        /// GetAll
        /// Select all student that existing in our DB
        /// </summary>
        /// <returns>IEnumerable list of students</returns>
        public IEnumerable<Student> GetAll()
        {
            return _universityContext.Students;
        }

        /// <summary>
        /// Get
        /// Select student by id - primary key
        /// </summary>
        /// <param name="id">Primary key of student</param>
        /// <returns>Unique student</returns>
        public Student Get(int id)
        {
            return _universityContext.Students.Find(id);
        }

        /// <summary>
        /// Create
        /// Add new student to DB
        /// </summary>
        /// <param name="item">Object of student</param>
        public void Create(Student item)
        {
            _universityContext.Students.Add(item);
        }

        /// <summary>
        /// Update
        /// Modify current student
        /// </summary>
        /// <param name="item">Object of student that should be updated</param>
        public void Update(Student item)
        {
            _universityContext.Students.AddOrUpdate(item);
        }

        /// <summary>
        /// Delete
        /// Remove student by id-primary key
        /// </summary>
        /// <param name="id">Primary key of student</param>
        public void Delete(int id)
        {
            var student = _universityContext.Students.Find(id);
            if (student != null)
                _universityContext.Students.Remove(student);
        }
    }
}
