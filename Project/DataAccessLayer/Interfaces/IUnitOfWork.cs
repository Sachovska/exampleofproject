﻿using Entities.DBModels;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// The interface will be used on UnitOfWork class that will be implement current repositories
    /// And genral methods Dispose/Save
    /// We can only get object of Repository
    /// </summary>
    public interface IUnitOfWork
    {
        IBaseRepository<Group> GroupRepository { get; }
        IBaseRepository<Student> StudentRepository { get; }

        void Dispose();
        int Save();
    }
}
