﻿using System.Collections.Generic;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// This interface will be used in all Repositories that should have this behavor
    /// </summary>
    /// <typeparam name="T">Instead of it add name of DB model</typeparam>
    public interface IBaseRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
