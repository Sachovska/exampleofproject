if not exists(select 1 from sys.databases s where s.name = 'University')
	create database[University]
go

use[University]
go


if not exists(select 1 from sys.objects s where s.name = 'Groups' and s.type = 'U')
	create table [dbo].[Groups]
	(
		[Id] int identity(1,1) not null
		,[Name] nvarchar(50) not null
		,constraint [uq_Group_Name] UNIQUE ([Name])
		,constraint [pk_Group] primary key ([Id])
	)
go

if not exists(select 1 from sys.objects s where s.name = 'Students' and s.type = 'U')
	create table [dbo].[Students]
	(
		[Id] int identity(1,1) not null
		,[Name] nvarchar(120) not null
		,[Surname] nvarchar(120) not null
		,[DateOfBirthday] date not null
		,[Gender] char(1) not null
		,[GroupId] int not null
		,constraint [pk_Student_Id] primary key ([Id])
		,check([Gender]='f' or [Gender]='m')
		,check([DateOfBirthday] <= CAST (GETDATE() AS DATE))
		,constraint [fk_Group_Id] foreign key ([GroupId]) references [dbo].[Groups] (Id)
	)
go